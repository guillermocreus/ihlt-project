from sklearn.pipeline import Pipeline

from src.data_processing.char_cleaner import CharCleaner
from src.data_processing.contranctions_remover import ContractionRemover
from src.data_processing.currency_modifier import CurrencyModifier
from src.data_processing.join_compounds_words import JoinCompoundWords
from src.data_processing.join_pos_lemma import JoinPosLema
from src.data_processing.lemmatizer import Lemmatizer
from src.data_processing.stopword_remover import StopWordsRemover
from src.data_processing.tag_pos import TagPOS
from src.data_processing.tokenizer import Tokenizer
# from char_cleaner import CharCleaner
# from contranctions_remover import ContractionRemover
# from join_compounds_words import JoinCompoundWords
# from src.data_processing.join_pos_lemma import JoinPosLema
# from tokenizer import Tokenizer
# from currency_modifier import CurrencyModifier
# from tag_pos import TagPOS
# from stopword_remover import StopWordsRemover
# from lemmatizer import Lemmatizer
# from sklearn.pipeline import Pipeline
# from join_pos_lemma import JoinPosLema

columns = ['sentence1', 'sentence2']
pos = [f'pos_{c}' for c in columns]
no_st_pos = [f'no_st_pos_{c}' for c in columns]
lemma = [f'lemma_{c}' for c in no_st_pos]

data_prcocessing_pipeline = Pipeline(steps=[
    ("char_cleaner", CharCleaner(columns)),
    ("currency_modifier", CurrencyModifier(columns)),
    ("contranctions_remover", ContractionRemover(columns)),
    ("tokenizre", Tokenizer(columns)),
    ("join_compound_words", JoinCompoundWords(col1=columns[0], col2=columns[1])),
    ("pos_tag", TagPOS(columns)),
    ("stop_words_remove", StopWordsRemover(columns=columns,pos_colums=pos)),
    ("lemmatizer", Lemmatizer(['pos_sentence1','pos_sentence2','no_st_pos_sentence1','no_st_pos_sentence2'])),
    ("lemma_pos_tag", JoinPosLema(columns=[(lemma[0],no_st_pos[0]),(lemma[1],no_st_pos[1])])),
])
