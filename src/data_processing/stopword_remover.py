from sklearn.base import BaseEstimator, TransformerMixin

stop_words = set([
    "i", "a", "about", "an", "are", "as", "at", "be", "by", "for", "from",
    "how", "in", "is", "it", "of", "on", "or", "that", "the", "this", "to",
    "was", "what", "when", "where", "who", "will", "with", "the", "'s", "did",
    "have", "has", "had", "were", "'ll"
])


class StopWordsRemover(BaseEstimator, TransformerMixin):

    """
        this class will remove stop words from pos taggs
        and tokenizzed string.
        We do this in this way in order to avoid things like Mark - mark

    """

    def __init__(self, pos_colums=[], columns=[]):
        self.columns = columns
        self.pos_colums = pos_colums

    def fit(self, x, y=None):
        return self

    def _process_tokenized_sentence(self, tokenized_sentence):
        """
            will parse tokenized  columns and will put inputs top lower case
            and will filter out the stop words 
        """
        temp = map(lambda word: word.lower(), tokenized_sentence)
        temp = filter(lambda word: word not in stop_words, temp)
        return list(temp)


    def _process_pos_sentence(self, pos_sentence):
        """
            will parse post tagged columns and will put inputs top lower case
            and will filter out the stop words pairs
        """
        temp = map(lambda pair: (pair[0].lower(), pair[1]), pos_sentence)
        temp = filter(lambda pair: pair[0] not in stop_words, temp)
        return list(temp)

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        #
        for column in self.columns:
            new_x[f'no_st_{column}'] = new_x[column].apply(lambda sentence: self._process_tokenized_sentence(sentence))

        for column in self.pos_colums:
            new_x[f'no_st_{column}'] = new_x[column].apply(lambda sentence: self._process_pos_sentence(sentence))
        return new_x
