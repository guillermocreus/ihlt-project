
from sklearn.base import BaseEstimator, TransformerMixin



class JoinPosLema(BaseEstimator, TransformerMixin):

    def __init__(self, columns=[]):
        """
            it expects a pair/touple where firs element is lemma array and second is pos array
        """
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def _join_lemma_pos(self, lema, pos):
        # it will create (lema,pos_tag_pair)
        zipped = [(l,p) for l,p in zip(lema, pos)]
        return list(map(lambda element: (element[0], element[1][1]), zipped))

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        for index, pair in enumerate(self.columns):
            new_x[f'lemma_pos_{index+1}'] = x[[pair[0], pair[1]]].apply(lambda row: self._join_lemma_pos( *row), axis=1)

        return new_x


# jcd = JoinPosLema(pairs=[('lemma_1', 'pos_1'),('lemma_2', 'pos_2')])
# jcd.fit_transform(df.copy())
# df[['lemma_1', 'pos_1']].apply(lambda row: len(*row), axis=1)
