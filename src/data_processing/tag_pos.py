
from sklearn.base import BaseEstimator, TransformerMixin
import nltk


class TagPOS(BaseEstimator, TransformerMixin):
    """
        POS Tag a column
        required nltk.download('averaged_perceptron_tagger')
    """

    def __init__(self, columns=[]):
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for col in self.columns:
            new_x[f'pos_{col}'] = new_x[col].apply(lambda element: nltk.pos_tag(element))
        return new_x
