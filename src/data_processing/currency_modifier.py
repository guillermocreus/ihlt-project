from nltk.tokenize import TreebankWordTokenizer
from sklearn.base import BaseEstimator, TransformerMixin
import re
import pandas as pd


class CurrencyModifier(BaseEstimator, TransformerMixin):
    """
        Will update currency from format [$USD1230] to [$1230]
        supported currency $ € £ 
    """

    def __init__(self, columns=[]):
        self.currencies = [
            (r'[$]US', '$'), (r'[€]EU', '€'), (r'[£]GBP', '£')
        ]
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def _parse(self, input):
        tmp = input
        for patter, replacement in self.currencies:
            tmp = re.sub(patter, replacement, tmp)
        return tmp

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for column in self.columns:
            new_x[column] = new_x[column].apply(
                lambda element: self._parse(element))
        return new_x
