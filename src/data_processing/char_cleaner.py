from sklearn.base import BaseEstimator, TransformerMixin
import re


class CharCleaner(BaseEstimator, TransformerMixin):
    """
        Will remove characters from strings
        for removing any other characters update the pattern
    """

    def __init__(self, columns=[], pattern=r'[-\/\\{}<>(),]'):
        self.pattern = pattern
        self.columns = columns

    def fit(self, x, y=None):
        return self


    def formater(self, sentence):
        try:
            return re.sub(self.pattern, '', sentence)
        except:
            print(sentence)
            return ''

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for column in self.columns:
            new_x[column] = new_x[column].apply(
                    lambda sentence:  self.formater(sentence))

        # new_x = new_x.applymap(lambda element: re.sub(self.pattern,'',element), na_action='ignore')
        return new_x
