from sklearn.base import BaseEstimator, TransformerMixin
import re


class ContractionRemover(BaseEstimator, TransformerMixin):
    """
        It will remove the contraction from the imput
        it assumes that input is a string 
    """

    def __init__(self, columns=[]):
        self.tuples = [(r'n\'t', ' not'), (r'\'m', ' am'),
                       (r'\'ve', ' have'), (r'\'re', ' are')]
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def _parse(self, input):
        tmp = input
        for patter, replacement in self.tuples:
            tmp = re.sub(patter, replacement, tmp)
        return tmp

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for column in self.columns:
            new_x[column] = new_x[column].apply(
                lambda sentence: self._parse(sentence))
        return new_x
