from sklearn.base import BaseEstimator, TransformerMixin


class JoinCompoundWords(BaseEstimator, TransformerMixin):
    """
    Will merge compound words from string 
    """

    def __init__(self, col1, col2):
        self.col1 = col1
        self.col2 = col2

    def fit(self, x, y=None):
        return self

    def join_compound_words(self, row):
        sa, sb = row[0], row[1]
        sb = set(x.lower() for x in sb)

        ind_a = 0
        sa_mod = []
        while ind_a < len(sa):
            if (ind_a + 1) < len(sa):
                combined_word = (sa[ind_a] + sa[ind_a + 1]).lower()

                if combined_word in sb:
                    sa_mod.append(combined_word)
                    ind_a += 2
                    continue

            sa_mod.append(sa[ind_a])
            ind_a += 1

        return sa_mod

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        new_x[self.col1] = new_x[[self.col1, self.col2]].apply(
            lambda row: self.join_compound_words(row), axis=1)
        new_x[self.col2] = new_x[[self.col2, self.col1]].apply(
            lambda row: self.join_compound_words(row), axis=1)
        return new_x
