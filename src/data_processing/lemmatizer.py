from nltk.corpus.reader.wordnet import ADJ, ADV
from sklearn.base import BaseEstimator, TransformerMixin
from nltk.stem import WordNetLemmatizer


class Lemmatizer(BaseEstimator, TransformerMixin):
    """
    Will Lematize input
    """

    def __init__(self, columns=[]):
        self.lemmatizer = WordNetLemmatizer()
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def _lemmatize_pair(self, input):
        if input[1][0] in {'N', 'V'}:
            return self.lemmatizer.lemmatize(input[0].lower(), pos=input[1][0].lower())
        elif (input[1] == 'JJ'):
            return self.lemmatizer.lemmatize(input[0].lower(), pos=ADJ)

        elif (input[1] == 'RB'):
            return self.lemmatizer.lemmatize(input[0].lower(), pos=ADV)

        return input[0].lower()

    def _lemmatize_list(self, pos_list):
        return list(map(lambda pair: self._lemmatize_pair(pair), pos_list))

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for column in self.columns:
            new_x[f'lemma_{column}'] = new_x[column].apply(lambda element: self._lemmatize_list(element))
        return new_x
