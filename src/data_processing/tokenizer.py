from nltk.tokenize import TreebankWordTokenizer
from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd


class Tokenizer(BaseEstimator, TransformerMixin):
    """
        Will tokenize input
    """

    def __init__(self, columns=[]):
        self.tokenizer = TreebankWordTokenizer()
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for column in self.columns:
            new_x[column] = new_x[column].apply(
                lambda element: self.tokenizer.tokenize(element))
        return new_x
