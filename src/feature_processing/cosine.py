from sklearn.base import BaseEstimator,TransformerMixin
from src.utils.util_fun import remove_punctuation
from math import sqrt


def cosine_similarity(s1, s2,include_punctuation=True):
    try:
        if include_punctuation:
            s1 = set(s1)
            s2 = set(s2)
            return len(s1.intersection(s2)) / sqrt(len(s1) * len(s2))
        
        s1 = remove_punctuation(s1)
        s2 = remove_punctuation(s2)
        return len(s1.intersection(s2)) / sqrt(len(s1) * len(s2))
    except:

        return 0
    

class Cosine(BaseEstimator, TransformerMixin):
    def __init__(self, columns_pairs=[]):
        self.pairs = columns_pairs

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        for column_1, column_2 in self.pairs:
            #
            #   add dice distance with punctuation
            #
            dist_name = f'cosine_sim_{column_1}_{column_2}_[+]pnkt'
            new_x[dist_name] = x[[column_1, column_2]].apply(lambda row: cosine_similarity(*row, include_punctuation=True), axis=1)

        
            dist_name = f'cosine_sim_{column_1}_{column_2}_[-]pnkt'
            new_x[dist_name] = x[[column_1, column_2]].apply(lambda row:  cosine_similarity(*row, include_punctuation=False), axis=1)

            #
            #   add dice distance without punctuation
            #
            # sim_name = f'dice_sim_{column_1}_{column_2}_[-]pnkt'
            # new_x[sim_name] = new_x[dist_name].apply(lambda distance: (1-distance))

        return new_x


# csn = Cosine( columns_pairs=[('sentence1', 'sentence2'),('no_st_sentence1','no_st_sentence2'),('lemma_pos_sentence1','lemma_pos_sentence2'),('lemma_no_st_sentence1','lemma_no_st_sentence2')])
# csn.fit_transform(train_df.copy())