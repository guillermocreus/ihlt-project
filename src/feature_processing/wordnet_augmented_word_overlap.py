from nltk.corpus import wordnet as wn
from sklearn.base import BaseEstimator, TransformerMixin
import nltk
nltk.download('wordnet')


class WordNetAugmentedWordOverlap(BaseEstimator, TransformerMixin):
    """
    Will compute similarity of NGrams sets created
    from sentences
    """

    def __init__(self, col1, col2):
        self.col1 = col1
        self.col2 = col2

        self.path_sim_rem = {}

    def fit(self, x, y=None):
        return self

    def path_sim(self, w1, w2):
        try:
            if (w1 > w2):
                w1, w2 = w2, w1

            pair_w = (w1, w2)
            if (pair_w in self.path_sim_rem):
                return self.path_sim_rem.get(pair_w)

            if (w1 == w2):
                self.path_sim_rem[pair_w] = 1
                return 1

            path_sim_max = max([syn1.path_similarity(syn2)
                                for syn1 in wn.synsets(w1)
                                for syn2 in wn.synsets(w2)
                                ] + [0.])

            self.path_sim_rem[pair_w] = path_sim_max
            return path_sim_max
        except:
            print('auch there is 0')
            return 0

    def wordnet_augmented_word_overlap(self, sentence1, sentence2):
        try:
            total_score_1 = 0
            for w1 in sentence1:
                total_score_1 += max([self.path_sim(w1, w2) for w2 in sentence2])
            total_score_1 /= len(sentence1)

            total_score_2 = 0
            for w2 in sentence2:
                total_score_2 += max([self.path_sim(w1, w2) for w1 in sentence1])
            total_score_2 /= len(sentence2)

            num = 2 * total_score_1 * total_score_2
            den = total_score_1 + total_score_2

            return num / den if den > 0 else 0
        except:
            print('WordNetAugmentedWordOverlap : there is a 0 ')
            return 0

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        new_x['wn_aug_word_overlap'] = new_x[[self.col1, self.col2]].apply(
            lambda row: self.wordnet_augmented_word_overlap(*row), axis=1)

        return new_x
