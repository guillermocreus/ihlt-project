from sklearn.base import BaseEstimator, TransformerMixin
from src.utils.util_fun import remove_punctuation
import Levenshtein as lev


def lev_ration(x, y, include_punctuation=True):
    if include_punctuation:
        return lev.seqratio(x, y)
    else:
        no_pnkt_x = list(remove_punctuation(x))
        no_pnkt_y = list(remove_punctuation(y))
        return lev.seqratio(no_pnkt_x, no_pnkt_y)


class LevenshteinDistance(BaseEstimator, TransformerMixin):
    def __init__(self, columns_pairs=[]):
        self.pairs = columns_pairs

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        for column_1, column_2 in self.pairs:
            #
            #   add Levenshtein setration with punctuation
            #
            dist_name = f'lev_setration_{column_1}_{column_2}_[+]pnkt'
            new_x[dist_name] = x[[column_1, column_2]].apply(
                lambda row: lev_ration(*row, include_punctuation=True), axis=1)
            #
            #   add Levenshtein setration without punctuation
            #
            # dist_name = f'lev_setration_{column_1}_{column_2}_[-]pnkt'
            # new_x[dist_name] = x[[column_1, column_2]].apply(
            #     lambda row: lev_ration(*row, include_punctuation=False), axis=1)
        return new_x


# lvn_dist = LevenshteinDistance(
#     columns_pairs=[('sentence1','sentence2'),('no_st_sentence1','no_st_sentence2'),('lemma_no_st_sentence1','lemma_no_st_sentence2')]
# )
# lvn_dist.fit_transform(df_copy.copy())
