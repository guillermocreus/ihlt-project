from sklearn.base import BaseEstimator, TransformerMixin
from src.utils.corpus import nyt_corpus, wiki_corpus


class CorpusSimilairities(BaseEstimator, TransformerMixin):
    def __init__(self, columns=[]):
        """
            colums is expected to be a list of tuples
        """
        self.pairs = columns

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for pair in self.pairs:
            if (pair == ('sentence1', 'sentence2')):
                new_x['weighted_word_match_f'] = x[[pair[0], pair[1]]].apply(
                    lambda row: nyt_corpus.get_weighted_word_match(*row), axis=1)
            else:
                new_x['weighted_word_match_l'] = x[[pair[0], pair[1]]].apply(
                    lambda row: nyt_corpus.get_weighted_word_match(*row), axis=1)

                new_x['nyt_sim'] = x[[pair[0], pair[1]]].apply(
                    lambda row: nyt_corpus.get_similarity(*row), axis=1)
                new_x['wiki_sim'] = x[[pair[0], pair[1]]].apply(
                    lambda row: wiki_corpus.get_similarity(*row), axis=1)

                new_x['nyt_w_sim'] = x[[pair[0], pair[1]]].apply(
                    lambda row: nyt_corpus.get_weighted_similarity(*row), axis=1)
                new_x['wiki_w_sim'] = x[[pair[0], pair[1]]].apply(
                    lambda row: wiki_corpus.get_weighted_similarity(*row), axis=1)
        return new_x


# cs = CorpusSimilairities(columns=[('lemma_1', 'lemma_2')])
# cs.transform(df.copy())
