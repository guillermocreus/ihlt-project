from sklearn.base import BaseEstimator, TransformerMixin
from src.utils.corpus import nyt_corpus


class RelativeLen(BaseEstimator, TransformerMixin):
    def __init__(self, columns_no_stop_word=[], columns_with_stop_word=[]):
        """
            colums is expected to be a list of tuples
        """
        self.columns_no_stop_word = columns_no_stop_word
        self.columns_with_stop_word = columns_with_stop_word
        self.min_weight = min(nyt_corpus.word_weight_table.values())

    def fit(self, x, y=None):
        return self

    def relative_len_diff(self, sentence1, sentence2):
        l1, l2 = len(sentence1), len(sentence2)
        return abs(l1 - l2) / float(max(l1, l2) + 1e-5)

    def relative_ic_diff(self, sentence1, sentence2):
        weights_1 = sum(max(0., nyt_corpus.word_weight_table[x] - self.min_weight) for x in sentence1)
        weights_2 = sum(max(0., nyt_corpus.word_weight_table[x] - self.min_weight) for x in sentence2)
        return abs(weights_1 - weights_2) / (max(weights_1, weights_2) + 1e-5)

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for pair in self.columns_no_stop_word:
            new_x[f"rel_len_{pair[0]}_{pair[1]}"] = x[[pair[0], pair[1]]].apply(lambda row: self.relative_len_diff(*row), axis=1)

        for pair in self.columns_with_stop_word:
            new_x[f'rel_ic_{pair[0]}_{pair[1]}'] = x[[pair[0], pair[1]]].apply(lambda row: self.relative_ic_diff(*row), axis=1)

        return new_x

