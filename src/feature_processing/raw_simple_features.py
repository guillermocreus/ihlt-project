from numpy import result_type
from sklearn.base import BaseEstimator, TransformerMixin
import re
from math import log


class RawSimpleFeatures(BaseEstimator, TransformerMixin):
    def __init__(self, columns):
        """
            colums is expected to be a list of tuples
        """
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def match_number(self, xa, xb):
        if xa == xb:
            return True
        xa = xa.replace(',', '')
        xb = xb.replace(',', '')

        try:
            va = int(float(xa))
            vb = int(float(xb))
            if (va == 0 or vb == 0) and va != vb:
                return False
            fxa = float(xa)
            fxb = float(xb)
            if abs(fxa - fxb) > 1:
                return False
            diga = xa.find('.')
            digb = xb.find('.')
            diga = 0 if diga == -1 else len(xa) - diga - 1
            digb = 0 if digb == -1 else len(xb) - digb - 1
            if diga > 0 and digb > 0 and va != vb:
                return False
            dmin = min(diga, digb)
            if dmin == 0:
                if abs(round(fxa, 0) - round(fxb, 0)) < 1e-5:
                    return True
                return va == vb
            return abs(round(fxa, dmin) - round(fxb, dmin)) < 1e-5
        except:
            pass

        return False

    def all_number_features(self, sentence1, sentence2):

        has_num = re.compile(r'^[0-9,./-]+$')
        has_digit = re.compile(r'[0-9]')

        numbers1 = set(w for w in sentence1 if has_num.match(w) and
                       has_digit.match(w))
        numbers2 = set(w for w in sentence2 if has_num.match(w) and
                       has_digit.match(w))
        intersection = 0
        for number_s1 in numbers1:
            if number_s1 in numbers2:
                intersection += 1
                continue
            for number_s2 in numbers2:
                if self.match_number(number_s1, number_s2):
                    intersection += 1
                    break

        len1, len2 = len(numbers1), len(numbers2)

        f = 1
        subset = 0
        if (len1 + len2 > 0):
            if (intersection == len1 or intersection == len2):
                subset = 1
            if (intersection > 0):
                p = intersection / len1
                r = intersection / len2
                f = 2 * p * r / (p + r)
            else:
                f = 0
        return (log(1 + len1 + len2), f, subset)

    def all_case_features(self, sentence1, sentence2):
        cases1 = set(w for w in sentence1[1:] if w[0].isupper()
                and w[-1] != '.')
        cases2 = set(w for w in sentence2[1:] if w[0].isupper()
                and w[-1] != '.')
        len1 = len(cases1)
        len2 = len(cases2)
        intersection = len(cases1.intersection(cases2))

        f1 = 1
        if (len1 > 0 and len2 > 0):
            if intersection > 0:
                p = intersection / len1
                r = intersection / len2
                f1 = 2 * p * r / (p + r)
            else:
                f1 = 0
        return (log(1 + len1 + len2), f1)

    def is_stock_tick(self, w):
        return (w[0] == '.' and len(w) > 1 and w[1:].isupper())

    def all_stock_features(self, sentence1, sentence2):
        ticks1 = set(w for w in sentence1 if self.is_stock_tick(w))
        ticks2 = set(w for w in sentence2 if self.is_stock_tick(w))
        intersection = len(ticks1.intersection(ticks2))
        len1 = len(ticks1)
        len2 = len(ticks2)

        f1 = 1
        if (len1 > 0 and len2 > 0):
            if intersection > 0:
                p = intersection / len1
                r = intersection / len2
                f = 2 * p * r / (p + r)
            else:
                f1 = 0
        return (log(1 + len1 + len2), f1)

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        for pair in self.columns:
            new_x[['number_feature_1', 'number_feature_2', 'number_feature_3']] = x[[pair[0], pair[1]]].apply(
                lambda row: self.all_number_features(*row), axis=1, result_type='expand')

            new_x[['case_matches_1', 'case_matches_2']] = x[[pair[0], pair[1]]].apply(
                lambda row: self.all_case_features(*row), axis=1, result_type='expand')
            
            new_x[['stock_ticks_1', 'stock_ticks_2']] = x[[pair[0], pair[1]]].apply(
                lambda row: self.all_stock_features(*row), axis=1, result_type='expand')

        return new_x
