from sklearn.base import BaseEstimator,TransformerMixin
from src.utils.util_fun import remove_punctuation


def dice_distance(x, y,include_punctuation=True):
    if include_punctuation:
        st_x = set(x) 
        st_y = set(y)
        return 1 - 2*len(st_x.intersection(st_y)) / (len(st_x) + len(st_y))
    else:
        st_x = remove_punctuation(x)
        st_y = remove_punctuation(y)
        return 1 - 2*len(st_x.intersection(st_y)) / (len(st_x) + len(st_y))


class Dice(BaseEstimator, TransformerMixin):
    def __init__(self, columns_pairs=[]):
        self.pairs = columns_pairs

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        for column_1, column_2 in self.pairs:
            #
            #   add dice distance with punctuation
            #
            dist_name = f'dice_sim_{column_1}_{column_2}_[+]pnkt'
            new_x[dist_name] = x[[column_1, column_2]].apply(lambda row: 1 - dice_distance(*row, include_punctuation=True), axis=1)

            #
            #   add dice similarity with punctuation
            
            # sim_name = f'dice_sim_{column_1}_{column_2}_[+]pnkt'
            # new_x[sim_name] = new_x[dist_name].apply(lambda distance: (1-distance))

            #
            #   add dice distance without punctuation
            #
            dist_name = f'dice_sim_{column_1}_{column_2}_[-]pnkt'
            new_x[dist_name] = x[[column_1, column_2]].apply(lambda row: 1 - dice_distance(*row, include_punctuation=False), axis=1)

            #
            #   add dice distance without punctuation
            #
            # sim_name = f'dice_sim_{column_1}_{column_2}_[-]pnkt'
            # new_x[sim_name] = new_x[dist_name].apply(lambda distance: (1-distance))

        return new_x


# jcd = Dice([('tknz_1', 'tknz_2'),('lemma_1', 'lemma_2')])
# jcd.fit_transform(df.copy())