from src.feature_processing.corpus_similarities import CorpusSimilairities
from src.feature_processing.relative_len import RelativeLen
from src.feature_processing.dice import Dice
from src.feature_processing.jaccard import Jacard
from src.feature_processing.ngram_feature import NGram
from src.feature_processing.wordnet_augmented_word_overlap import WordNetAugmentedWordOverlap
from src.feature_processing.weighted_word_overlap import WeightedWordOverlap
from src.feature_processing.levenshtein_distance import LevenshteinDistance
from src.feature_processing.raw_simple_features import RawSimpleFeatures
from sklearn.pipeline import Pipeline


columns_words = ['no_st_sentence1', 'no_st_sentence1']
columns_lemmas = ['lemma_no_st_sentence1', 'lemma_no_st_sentence2']

feature_pipline = Pipeline(steps=[
    ('corpus_similarities', CorpusSimilairities(
        columns=[('lemma_no_st_sentence1', 'lemma_no_st_sentence2'), ('sentence1', 'sentence2')])
     ),

    ('raw_features', RawSimpleFeatures(
        columns=[('sentence1', 'sentence2')])
     ),

    ('realative', RelativeLen(
        columns_no_stop_word=[('no_st_sentence1', 'no_st_sentence2')],
        columns_with_stop_word=[('sentence1', 'sentence1')]
    )),

    ('dice', Dice(
        columns_pairs=[
            ('sentence1', 'sentence2'), ('no_st_sentence1', 'no_st_sentence2'),  ('lemma_no_st_sentence1', 'lemma_no_st_sentence2')]
    )),
    ('jaccard', Jacard(
        columns_pairs=[('sentence1', 'sentence2'), ('no_st_sentence1',
                                                    'no_st_sentence2'), ('lemma_no_st_sentence1', 'lemma_no_st_sentence2')]
    )),
    ('levenshtein', LevenshteinDistance(
        columns_pairs=[('sentence1', 'sentence2'), ('no_st_sentence1',
                                                    'no_st_sentence2'), ('lemma_no_st_sentence1', 'lemma_no_st_sentence2')]
    )),

    ("ngram_feature_1_w", NGram(
        1, columns_words[0], columns_words[1], lemmas=False)),
    ("ngram_feature_2_w", NGram(
        2, columns_words[0], columns_words[1], lemmas=False)),
    ("ngram_feature_3_w", NGram(
        3, columns_words[0], columns_words[1], lemmas=False)),

    ("ngram_feature_1_l", NGram(1, 'lemma_pos_1', 'lemma_pos_2', lemmas=True)),
    ("ngram_feature_2_l", NGram(2, 'lemma_pos_1', 'lemma_pos_2', lemmas=True)),
    ("ngram_feature_3_l", NGram(3, 'lemma_pos_1', 'lemma_pos_2', lemmas=True)),

    ("wordnet_augmented_word_overlap", WordNetAugmentedWordOverlap(columns_lemmas[0],
                                                                   columns_lemmas[1])),
    ("wordnet_weighted_word_overlap_lemmas", WeightedWordOverlap(
        columns_lemmas[0], columns_lemmas[1], column_name='wn_weighted_word_overlap_l')),
    ("wordnet_weighted_word_overlap_words", WeightedWordOverlap(
        columns_words[0], columns_words[1], column_name='wn_weighted_word_overlap_w')),

])

# features = feature_pipline.fit_transform(df_copy.copy())

# features = features.drop(df_copy.columns.values, axis=1)
# print(features.shape)
# features.head()
