from nltk.corpus import wordnet as wn
from sklearn.base import BaseEstimator, TransformerMixin
from math import log
from collections import defaultdict
import nltk
nltk.download('wordnet')


class WeightedWordOverlap(BaseEstimator, TransformerMixin):
    """
    Will compute similarity of NGrams sets created
    from sentences
    """

    def __init__(self, col1, col2, column_name):
        self.col1 = col1
        self.col2 = col2
        self.column_name = column_name

        self.word_weight = self.load_word_weight_table(
            'data/word-frequencies.txt')

    def fit(self, x, y=None):
        return self

    def load_word_weight_table(self, path):
        lines = open(path).readlines()
        word_weight = defaultdict(float)
        totfreq = int(lines[0])
        for l in lines[1:]:
            w, freq = l.split()
            freq = float(freq)
            if freq < 10:
                continue
            word_weight[w] = log(totfreq / freq)

        return word_weight

    def wwc(self, set1, set2):
        try:
            num = 0
            den = 0
            for w in set2:
                den += self.word_weight[w]
                if w in set1:
                    num += self.word_weight[w]

            score_s1_s2 = num / den

            num = 0
            den = 0
            for w in set1:
                den += self.word_weight[w]
                if w in set2:
                    num += self.word_weight[w]

            score_s2_s1 = num / den

            if (score_s1_s2 + score_s2_s1 > 0):
                return 2 * (score_s1_s2 * score_s2_s1) / (score_s1_s2 + score_s2_s1)
            return 0
        except:
            print('WeightedWordOverlap : there is a 0 ')
            return 0

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()
        new_x[self.column_name] = new_x[[self.col1, self.col2]].apply(
            lambda row: self.wwc(*row), axis=1)

        return new_x
