from nltk.metrics import jaccard_distance
import pandas as pd
from src.utils.util_fun import remove_punctuation
from sklearn.base import BaseEstimator, TransformerMixin


def jaccard_distancea(a, b, include_punctuation=False):
    return jaccard_distance(set(a), set(b)) if include_punctuation else jaccard_distance(remove_punctuation(a), remove_punctuation(b))


class Jacard(BaseEstimator, TransformerMixin):
    def __init__(self, columns_pairs=[]):
        self.columns_pairs = columns_pairs

    def fit(self, x, y=None):
        return self

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        for column_1, column_2 in self.columns_pairs:
            #
            #   add jaccard distance with punctuation
            #
            dist_name = f'jcd_sim_{column_1}_{column_2}_[+]pnkt'
            new_x[dist_name] = x[[column_1, column_2]].apply(lambda row: 1 - jaccard_distancea(*row, include_punctuation=True), axis=1)

            #
            #   add jaccard similarity with punctuation
            #
            # sim_name = f'jcf_sim_{column_1}_{column_2}_[+]pnkt'
            # new_x[sim_name] = new_x[dist_name].apply(lambda distance: (1-distance))

            #
            #   add jaccard distance without punctuation
            #
            # dist_name = f'jcd_dist_{column_1}_{column_2}_[-]pnkt'
            # new_x[dist_name] = x[[column_1, column_2]].apply(lambda row: 1 - jaccard_distancea(*row, include_punctuation=False), axis=1)

            #
            #   add jaccard distance without punctuation
            #
            # sim_name = f'jcf_sim_{column_1}_{column_2}_[-]pnkt'
            # new_x[sim_name] = new_x[dist_name].apply(lambda distance: (1-distance))

        return new_x


# jcd = Jacard([('lemma_1', 'lemma_2'), ('tknz_1', 'tknz_2')])
# jcd.fit_transform(df.copy())
