from sklearn.base import BaseEstimator, TransformerMixin
from collections import Counter


class NGram(BaseEstimator, TransformerMixin):
    """
    Will compute similarity of NGrams sets created
    from sentences
    """

    def __init__(self, n, col1, col2, lemmas):
        self.n = n
        self.col1 = col1
        self.col2 = col2

        self.valid_pos = {'JJ', 'JJR', 'JJS', 'NN', 'NNP', 'NNS', 'NNPS', 'RB',
                          'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ'}
        self.lemmas = lemmas

    def fit(self, x, y=None):
        return self

    def ngrams(self, element):
        ngrams = []
        for ind in range(len(element) - self.n + 1):
            # One can only add non-hashable elements to a set
            # That's why we transform the list to tuple

            if (self.lemmas):
                ngrams.append(
                    tuple([word for (word, pos) in element[ind:(
                        ind + self.n)] if pos in self.valid_pos])
                )
            else:
                ngrams.append(
                    tuple(element[ind:(ind + self.n)])
                )

        return ngrams

    def compute_feature(self, elem1, elem2):
        list1, list2 = self.ngrams(elem1), self.ngrams(elem2)

        c1 = Counter(list1)
        pos = 0

        for ngram in list2:
            if (c1[ngram]):
                c1[ngram] -= 1
                pos += 1

        a, b = 0, 0
        res = 1

        if (len(list1) > 0 and len(list2) > 0):
            a = pos / len(list1)
            b = pos / len(list2)
            res = 2 / (1 / a + 1 / b) if (a * b != 0) else 0

        return res

    def transform(self, x, y=None, **fit_params):
        new_x = x.copy()

        col_name = f"{self.n}gram_overlap_"
        col_name += "l" if self.lemmas else "w"
        new_x[col_name] = new_x[[self.col1, self.col2]].apply(
            lambda row: self.compute_feature(*row), axis=1)

        return new_x
