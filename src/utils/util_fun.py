


from string import punctuation

punctuation = set([value for value in punctuation])


def remove_punctuation(input):
    """
        it remove punctuation from set and return a filtered set
    """
    return set(filter(lambda element: element not in punctuation, input))