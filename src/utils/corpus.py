import math
from collections import defaultdict
import numpy as np
from numpy.linalg import norm
from collections import Counter


def _load_word_weights_table(path):
    """

    """
    file_lines = open(path).readlines()
    words_weights = defaultdict(float)

    if not len(file_lines):
        return (words_weights, 0.)

    total_frequency = int(file_lines[0])

    for line in file_lines[1:]:
        word, frequency = line.split()
        frequency = float(frequency)
        if frequency < 10:
            continue
        words_weights[word] = math.log(total_frequency / frequency)

    return words_weights


class Corpus:
    def __init__(self, words, vectors):
        self.word_weight_table = _load_word_weights_table(
            'data/word-frequencies.txt')
        self.word_to_idx = {word: index for index,
                            word in enumerate(w.strip() for w in open(words))}
        self.mat = np.loadtxt(vectors)

    def _bag_of_word_to_vector(self, input):
        vec = np.zeros(self.mat.shape[1])
        for key, value in input.items():
            index = self.word_to_idx.get(key, -1)
            if index >= 0:
                vec += self.mat[index] / (norm(self.mat[index]) + 1e-8) * value
        return vec

    def get_weighted_word_match(self, sentence1, sentence2):
        counter1 = Counter(sentence1)
        counter2 = Counter(sentence2)
        
        sum_of_weights_1 = sum(
            self.word_weight_table[w] * counter1[w] for w in counter1)
        sum_of_weights_2 = sum(
            self.word_weight_table[w] * counter2[w] for w in counter2)

        w_sum = 0

        for w in counter1:
            w_min_count = min(counter1[w], counter2[w])
            w_sum += self.word_weight_table[w] * w_min_count

        p = 0
        r = 0

        if (sum_of_weights_1 > 0 and w_sum > 0):
            p = w_sum / sum_of_weights_1

        if (sum_of_weights_2 > 0 and w_sum > 0):
            r = w_sum / sum_of_weights_2

        f1 = 2 / (1 / p + 1 / r) if (p*r != 0) else 0

        return f1

    def _weighted_sentence_to_dict(self, sentence):
        counter = Counter(sentence)
        return {word: (self.word_weight_table[word] * counter[word]) for word in counter}

    def get_weighted_similarity(self, sentence1, sentence2):
        sentence_1_vector = self._bag_of_word_to_vector(
            self._weighted_sentence_to_dict(sentence1))
        sentence_2_vector = self._bag_of_word_to_vector(
            self._weighted_sentence_to_dict(sentence2))
        return abs(sentence_1_vector.dot(sentence_2_vector) / (norm(sentence_1_vector) + 1e-8) / (norm(sentence_2_vector) + 1e-8))

    def _sentence_to_dict(self, sentence):
        return {word: 1 for word in Counter(sentence)}

    def get_similarity(self, sentence1, sentence2):
        sentence_1_vector = self._bag_of_word_to_vector(
            self._sentence_to_dict(sentence1))
        sentence_2_vector = self._bag_of_word_to_vector(
            self._sentence_to_dict(sentence2))
        return abs(sentence_1_vector.dot(sentence_2_vector) / (norm(sentence_1_vector) + 1e-8) / (norm(sentence_2_vector) + 1e-8))


def load_nyt_corpus():
    return Corpus('data/nyt_words.txt', 'data/nyt_word_vectors.txt')


def load_wiki_corpus():
    return Corpus('data/wikipedia_words.txt', 'data/wikipedia_word_vectors.txt')


nyt_corpus = load_nyt_corpus()
wiki_corpus = load_wiki_corpus()
