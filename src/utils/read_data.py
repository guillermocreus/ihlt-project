
import pandas as pd

def read_data(path_df, path_gs):
    df = pd.read_csv(path_df, sep='\t', header=None, error_bad_lines=False)
    df['gs'] = pd.read_csv(path_gs, sep='\t', header=None)
    df.rename(columns={0: 'sentence1', 1: 'sentence2'}, inplace=True)

    return df
